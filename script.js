// 1
// Опишіть своїми словами, що таке екранування,
// і навіщо воно потрібне в мовах програмування
//
// Экранування додається для того щоб унеможливіти
// некорректну інтерпретнацію коду.

// 2
// Які засоби оголошення функцій ви знаєте?
//
// ключове слово Function потім назва та параметри,
// після параметрів криві душки. Функція також може
// бути анонімна та без заданних параметрів на початку

// 3
//
// Підняття або hoisting - це
// механізм JavaScript, в якому змінні і оголошення функцій,
//  пересуваються вгору своєї області видимості перед тим,
//  як код буде виконано. І так ми бічимо що не вадливо де
// були оголошенні змінні та має разніці яка зона бачення
//локальна чи глобальна

// ===========================

function createNewUser(
  userFirstName = prompt("Напишіть будь-ласка Ваше імя"),
  userLastName = prompt("Напишіть будь-ласка своє призвище"),
  userBirthday = prompt(
    "Напішить будь-ласка дату народження у форматі dd.mm.yyyy"
  )
) {
  this._firstName = userFirstName;
  this._lastName = userLastName;
  this._birthday = new Date(
    userBirthday.slice(6, 10),
    userBirthday.slice(3, 5) - 1,
    userBirthday.slice(0, 2)
  );

  this.getBirthday = function () {
    return this._birthday.toLocaleDateString();
  };

  this.getPassword = function () {
    return (
      this._firstName.charAt(0).toUpperCase() +
      this._lastName.toLowerCase() +
      this._birthday.getFullYear()
    );
  };

  this.getAge = function () {
    return (
      Math.floor(
        (new Date().getTime() - new Date(this._birthday)) /
          (24 * 3600 * 365.25 * 1000)
      ) + " years"
    );
  };

  this.getLogin = function () {
    return (this._firstName.charAt(0) + this._lastName).toLowerCase();
  };

  Object.defineProperty(this, "setFirstName", {
    set: function (value) {
      this._firstName = value;
    },
  }),
    Object.defineProperty(this, "setLastName", {
      set: function (value) {
        this._lastName = value;
      },
    });
}

let newUser = new createNewUser();

console.log(newUser);
console.log("========================");

console.log(newUser.getLogin());
console.log("========================");

console.log(newUser.getBirthday());
console.log("========================");

console.log(newUser.getAge());
console.log("========================");

console.log(newUser.getPassword());
console.log("========================");
